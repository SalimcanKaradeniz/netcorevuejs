﻿import Vue from 'vue'
import HomeComponent from './home.vue'
import ContactComponent from './contactus.vue'
import AboutComponent from './aboutus.vue'


var app = new Vue({
    el: '#app',
    data: {
        items: [
            { message: 'Foo' },
            { message: 'Bar' }
        ]
    }
});